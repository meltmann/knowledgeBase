# HexCrawl

Das Wichtigste ist die Vorbereitung mit der Storyvorbereitung, die zunächst gesucht werden muss.

Wobei das auch mit Quadraten statt Hexagonalen geht.

- Grob mal 10x10 Felder ausformulieren. 
- Zufallstabellen verwenden!
- Sowohl Umgebungen als auch Kämpfe als auch Items und geheimnisvolle Elemente…
- Hinweise streuen, die in die Geschichte passen
- Keinesfalls die Geschichte planen, nur die Spotlights
- Etwa 3 Storylines offen, die weiterlaufen.

Generell beschreibt der Spielleiter die Umgebung. Das Leben drum herum läuft weiter.

Zuhören und aufgreifen!

Hex-Feld hat etwa 20 Meilen oder 9 Kilometer "Durchmesser" und entspricht daher der üblichen Sichtweite.
Ließe sich das auf im Tabletop Simulator machen?

Hex-Feldkarte mit Startgebiet und optisch markanten Punkten wie Gebirge sind für die Spieler sichtbar.
Details werden dann halt aufgedeckt.

# Vorteile

Zusätzlicher Mechanismus, der quasi über das System gelegt wurde.

# Nachteile

Improvisationsdings brauchen zwingend Mitspielen von Allen.